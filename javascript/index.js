var net = require('net');
var util = require('util');
var _ = require('lodash');
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log('I\'m', botName, 'and connect to', serverHost + ':' + serverPort);

var joinRequest = {
  msgType: 'join',
  data: {
    name: botName,
    key: botKey
  }
};

var joinRaceRequest = {
  msgType: 'joinRace',
  data: {
    botId: {
      name: 'Animal Farm',
      key: botKey
    },
    trackName: 'keimola',
    // trackName: 'germany',
    // trackName: 'usa',
    // trackName: 'france',
    // password: 'roman0',
    carCount: 2
  }
};

var client = net.connect(serverPort, serverHost, function() {
  return send(joinRequest);
  // return send(joinRaceRequest);
});


var car = {};
var race = null;
var currentTick;
var RESISTANCE;
var maxAngle;
var ticksAfterStart;
var maxAngleAcceleration = 59 / 2.1 * Math.PI / 180;


function round(x) {

  return Math.round(x * 100) / 100;
}


function send(json) {

  client.write(JSON.stringify(json));
  return client.write('\n');
}


function printCar() {

  console.log(
    'd: ' + round(car.inPieceDistance) +
    '  l: ' + car.laneIndex +
    '  r: ' + round(car.radius) +
    '  tv: ' + round(car.targetSpeed) +
    '  v: ' + round(car.speed) +
    '  a: ' + round(car.acceleration) +
    '  thr: ' + round(car.throttle) +
    '  ang: ' + round(car.angle) +
    '  aa: ' + round(car.angleAcceleration) +
    '  dtc: ' + round(car.distanceToCar));
}


function resetCars() {
  car.pieceIndex = 0;
  car.lap = -1;
  car.inPieceDistance = 0;
  car.acceleration = 0;
  car.throttle = 0;
  car.speed = 0;
  car.angle = 0;
  car.angleAcceleration = 0;
  car.targetSpeed = 0;
  car.radius = 0;
  car.laneIndex = 0;
  car.switchSent = false;
  car.isTurboAvailable = false;
  car.isTurboAvailableScheduled = false;
  car.isTurboOn = false;
  car.crashes = 0;
  car.isAwaitingSpawn = 0;
  car.isHitting = 0;

  _.each(race.cars, function(raceCar) {

    raceCar.inPieceDistance = 0;
    raceCar.speed = 0;
    raceCar.acceleration = 0;
    raceCar.accelerationChange = 0;
    raceCar.angle = 0;
    raceCar.angleAcceleration = 0;
    raceCar.pieceIndex = -1;
    raceCar.lap = -1;
    raceCar.isCrashed = false;
  });
}


function getTargetSpeed(pieceIndex) {

  var piece = race.track.pieces[pieceIndex];
  var previousPiece = race.track.pieces[getNextPieceIndex(pieceIndex, -1)];
  var nextPiece = race.track.pieces[getNextPieceIndex(pieceIndex, 1)];

  if (!piece.radius) {
    return 100;
  }

  var currentLane = _.find(race.track.lanes, {index: car.laneIndex});

  var laneRadius = piece.angle > 0
    ? piece.radius - currentLane.distanceFromCenter
    : piece.radius + currentLane.distanceFromCenter;

  if ((!previousPiece.radius || previousPiece.angle * piece.angle < 0) &&
      (!nextPiece.radius || nextPiece.angle * piece.angle < 0)) {
    return Math.sqrt(laneRadius * maxAngleAcceleration * Math.sqrt(90 / Math.abs(piece.angle))) * RESISTANCE;
  }

  return Math.sqrt(maxAngleAcceleration * laneRadius) * RESISTANCE;
}


function createThrottleMessage(throttle) {
  return {
    msgType: 'throttle',
    data: throttle,
    gameTick: currentTick
  };
}

function getNextPieceIndex(current, i) {

  var index = current + i;
  return index >= 0
    ? index % race.track.pieces.length
    : race.track.pieces.length + index;
}


function distanceToNextSwitch(distanceFromCenter) {

  var distance = 0;
  var i = getNextPieceIndex(car.pieceIndex, 2); // First piece after the switch
  var piece = race.track.pieces[i];

  while (!piece.switch) {

    if (!piece.radius) {
      distance += piece.length;
    } else {
      var laneRadius = piece.angle > 0
        ? piece.radius - distanceFromCenter
        : piece.radius + distanceFromCenter;

      distance += 2 * Math.PI * laneRadius * Math.abs(piece.angle) / 360;
    }

    i = getNextPieceIndex(i, 1);
    piece = race.track.pieces[i];
  }

  return distance;
}


function getSwitchDirection(nextPiece) {

  if (!nextPiece.switch) {
    return '';
  }

  var currentLaneIndex = _.findIndex(race.track.lanes, {index: car.laneIndex});
  var currentLaneDistance = distanceToNextSwitch(race.track.lanes[currentLaneIndex].distanceFromCenter);
  var rightLane = race.track.lanes[currentLaneIndex + 1];
  var rightLaneDistance = rightLane && distanceToNextSwitch(rightLane.distanceFromCenter) || 10000;
  var leftLane = race.track.lanes[currentLaneIndex - 1];
  var leftLaneDistance = leftLane && distanceToNextSwitch(leftLane.distanceFromCenter) || 10000;

  if (rightLaneDistance < currentLaneDistance || leftLaneDistance < currentLaneDistance) {
    return rightLaneDistance < leftLaneDistance ? 'Right' : 'Left';
  }

  return '';
}


function isTooFastBeforTurn() {

  var nextTurnData = getNextTurnData();

  if (!nextTurnData) {
    return false;
  }

  var speedDiff = car.speed - nextTurnData.targetSpeed;

  return nextTurnData.distance > 50 &&
    speedDiff / nextTurnData.distance > 0.03 * 64.0 / (car.speed * car.speed) * RESISTANCE;
}


function isTooSharpAngle() {

  if (Math.abs(car.angleAcceleration) < 0.0001) {

    return false;
  }

  var criticalAngle = car.angleAcceleration > 0 ? 60 : - 60;
  var ticksToCrash = (criticalAngle - Math.abs(car.angle)) / car.angleAcceleration;

  return ticksToCrash < 8;
}


function shouldHit(cars) {

  for (var i = 0; i < cars.length; i++) {

    var result = shouldHitCar(cars[i]);

    if (result) {
      return result;
    }
  }

  return false;
}


function getDistanceToSwitch(piecePosition) {

  var distance = 0;
  var i = getNextPieceIndex(piecePosition.pieceIndex, 1);
  var piece = race.track.pieces[i];

  while (!piece.switch) {

    distance += getPieceLength(i);
    i = getNextPieceIndex(i, 1);
    piece = race.track.pieces[i];
  }

  return distance + getPieceLength(piecePosition.pieceIndex) - piecePosition.inPieceDistance;
}


function shouldHitCar(otherCar) {

  if (otherCar.id.color === car.color || otherCar.piecePosition.lane.endLaneIndex !== car.laneIndex) {

    return false;
  }

  var otherCarData = _.find(race.cars, function(carData) {return carData.id.color === otherCar.id.color;});

  if (otherCarData.isCrashed) {

    return false;
  }

  var distanceToCar = getDistance(otherCar.piecePosition);

  if (distanceToCar < 0) {

    return false;
  }

  var otherCarRearLenght = otherCarData.dimensions.length - otherCarData.dimensions.guideFlagPosition;
  var frontToRearDistance = distanceToCar - otherCarRearLenght - car.guideFlagPosition;
  car.distanceToCar = frontToRearDistance;

  if (frontToRearDistance < 0.0001) {

    return true;
  }

  var otherCarDistanceToSwitch = getDistanceToSwitch(otherCar.piecePosition);

  if (willCatchUp(frontToRearDistance, 0.2081, otherCarData, otherCarDistanceToSwitch)) {
    return true;
  }

  var piece = race.track.pieces[car.pieceIndex];

  if (!piece.radius &&
      car.isTurboAvailable &&
      !car.isTurboOn &&
      willCatchUp(frontToRearDistance, 0.2081 * 3, otherCarData, otherCarDistanceToSwitch)) {

    return 'turbo';
  }

  return false;
}


function willCatchUp(distance, maxAcceleration, otherCarData, otherCarDistanceToSwitch) {

  var s1 = 0;
  var v1 = car.speed;
  var s2 = 0;
  var v2 = otherCarData.speed;
  var a2 = otherCarData.acceleration;
  var angle = car.angle;
  var tickLimit = car.isHitting ? 20 : 12;

  for (var tick = 0; tick < tickLimit; tick++) {

    s1 += v1;
    v1 += (maxAcceleration - 0.0207 * v1);
    s2 += v2;
    v2 += a2;
    a2 += otherCarData.accelerationChange;

    if (!car.isHitting && otherCarDistanceToSwitch - s2 < 0) {
      return false;
    }

    if (distance - s1 + s2 < 0.0001) {
      return true;
    }

    angle = car.angle > 0
      ? angle + car.angleAcceleration
      : angle - car.angleAcceleration;

    if (Math.abs(angle) > 60) {
      return false;
    }

  }

  return false;
}


function getDistance(piecePosition) {

  var distance = 0;
  var i = car.pieceIndex;

  while (i !== piecePosition.pieceIndex) {

    distance += getPieceLength(i);
    i = getNextPieceIndex(i, 1);
  }

  return distance - car.inPieceDistance + piecePosition.inPieceDistance;
}


function turnTurboOn() {

  car.isTurboAvailable = false;
  car.isTurboOn = true;

  return {
    msgType: 'turbo',
    data: 'turbo',
    gameTick: currentTick
  };
}


function getMessage(cars) {

  car.isHitting = car.isHitting && car.acceleration > 0;

  var hit = shouldHit(cars);

  if (hit === 'turbo') {

    console.log('TURBO FOR HIT!!!');
    car.isHitting = true;
    return turnTurboOn();
  }

  if (hit === true) {

    console.log('SPEED FOR HIT!!!');
    car.isHitting = true;
    return createThrottleMessage(1);
  }

  car.isHitting = false;

  if (isTooSharpAngle()) {

    console.log('ANGLE!!!');
    return createThrottleMessage(0);
  }

  if (isTooFastBeforTurn()) {

    console.log('TOO FAST!');
    return createThrottleMessage(0);
  }

  if (!car.switchSent) {

    var switchDirection = getSwitchDirection(race.track.pieces[getNextPieceIndex(car.pieceIndex, 1)]);

    if (switchDirection) {

      car.switchSent = true;
      return {
        msgType: 'switchLane',
        data: switchDirection,
        gameTick: currentTick
      };
    }
  }

  if (car.isTurboAvailable && !car.isTurboOn && shouldTurbo()) {

    console.log('TURBO!!!');
    return turnTurboOn();
  }

  var K = 2.5 / 100 * RESISTANCE;
  var pieceIndexes = _.map([1, 2], function(i) { return getNextPieceIndex(car.pieceIndex, i); });
  var targetSpeeds = _.map(pieceIndexes, getTargetSpeed);
  var pieceLengths = _.map(pieceIndexes, getPieceLength);
  var targetSpeedAdjusted = [];
  var totalLength = 0;

  for (var i = 0; i < targetSpeeds.length; i++) {

    if (i > 0) {
      totalLength += pieceLengths[i - 1];
    }

    targetSpeedAdjusted.push(targetSpeeds[i] + K * totalLength);
  }

  car.targetSpeed = Math.min.apply(Math, targetSpeedAdjusted);

  return createThrottleMessage(maintainSpeed(car.targetSpeed));
}


function getNextTurnData() {

  var i = car.pieceIndex;
  var piece = race.track.pieces[i];

  if (piece.radius) {
    return false;
  }

  var straightDistance = piece.length - car.inPieceDistance;
  i = getNextPieceIndex(i, 1);
  piece = race.track.pieces[i];

  while (!piece.radius) {
    straightDistance += piece.length;
    i = getNextPieceIndex(i, 1);
    piece = race.track.pieces[i];
  }

  return {
    targetSpeed: getTargetSpeed(i),
    distance: straightDistance
  };
}


function shouldTurbo() {

  var nextTurnData = getNextTurnData();

  if (!nextTurnData) {
    return false;
  }

  return (nextTurnData.distance > 300) ||
    (nextTurnData.distance > 200) && (nextTurnData.targetSpeed - car.speed > 6.0);
}


function getPieceLength(pieceIndex) {

  var piece = race.track.pieces[pieceIndex];

  if (!piece.radius) {
    return piece.length;
  }

  var currentLane = _.find(race.track.lanes, {index: car.laneIndex});

  var laneRadius = piece.angle > 0
    ? piece.radius - currentLane.distanceFromCenter
    : piece.radius + currentLane.distanceFromCenter;

  return 2 * Math.PI * laneRadius * Math.abs(piece.angle) / 360;
}


function maintainSpeed(targetSpeed) {
  var speedDiff = targetSpeed - car.speed;

  if (Math.abs(car.acceleration) < 0.0001) {
    return speedDiff > 0
      ? Math.min(car.throttle + speedDiff, 1)
      : Math.max(car.throttle + speedDiff, 0);
  }

  var ticksToTargetSpeed = speedDiff / car.acceleration;

  if (ticksToTargetSpeed < 0) {
    return speedDiff > 0
      ? Math.min(car.throttle + speedDiff, 1)
      : Math.max(car.throttle + speedDiff, 0);
  }

  if (ticksToTargetSpeed > 10) {
    return speedDiff > 0
      ? Math.min(car.throttle + speedDiff, 1)
      : Math.max(car.throttle + speedDiff, 0);
  }

  if (ticksToTargetSpeed > 5) {
    return car.throttle;
  }

  return car.acceleration > 0
    ? Math.max(car.throttle - (car.acceleration * ticksToTargetSpeed / 5), 0)
    : Math.min(car.throttle - (car.acceleration * ticksToTargetSpeed / 5), 1);
}


function sendMessage(message) {

  if (message.msgType === 'throttle') {
    car.throttle = message.data;
  }

  send(message);
}


function updateOtherCars(data, time) {

  _.each(data, function(carData) {

    var car = _.find(race.cars, function(c) {return c.id.color === carData.id.color;});
    var piece = race.track.pieces[car.pieceIndex];

    var distance = car.pieceIndex !== carData.piecePosition.pieceIndex
      ? carData.piecePosition.inPieceDistance + (getPieceLength(car.pieceIndex) - car.inPieceDistance)
      : (carData.piecePosition.inPieceDistance - car.inPieceDistance);

    var newSpeed = car.pieceIndex !== carData.piecePosition.pieceIndex && piece.switch
      ? car.speed + car.acceleration
      : distance / time;

    var acceleration = (newSpeed - car.speed) / time;
    var accelerationChange = (acceleration - car.acceleration) / time;
    var angleAcceleration = (Math.abs(carData.angle) - Math.abs(car.angle)) / time;

    car.inPieceDistance = carData.piecePosition.inPieceDistance;
    car.speed = newSpeed;
    car.acceleration = acceleration;
    car.accelerationChange = accelerationChange;
    car.angle = carData.angle;
    car.angleAcceleration = angleAcceleration;
    car.pieceIndex = carData.piecePosition.pieceIndex;
    car.lap = carData.piecePosition.lap;
  });
}


function updateCar(data, time) {

  var myCar = _.find(data, function(carData) { return carData.id.color === car.color; });
  var piece = race.track.pieces[car.pieceIndex];

  var distance = car.pieceIndex !== myCar.piecePosition.pieceIndex
    ? myCar.piecePosition.inPieceDistance + (getPieceLength(car.pieceIndex) - car.inPieceDistance)
    : (myCar.piecePosition.inPieceDistance - car.inPieceDistance);

  var newSpeed = car.pieceIndex !== myCar.piecePosition.pieceIndex && piece.switch
    ? car.speed + car.acceleration
    : distance / time;

  if (newSpeed > 0.0001) {
    ticksAfterStart++;

    if (ticksAfterStart === 8) {
      RESISTANCE = Math.sqrt(1.40 / newSpeed);
    }
  }

  var acceleration = (newSpeed - car.speed) / time;
  var angleAcceleration = (Math.abs(myCar.angle) - Math.abs(car.angle)) / time;

  car.inPieceDistance = myCar.piecePosition.inPieceDistance;
  car.speed = newSpeed;
  car.acceleration = acceleration;
  car.angle = myCar.angle;

  if (Math.abs(car.angle) > maxAngle) {
    maxAngle = Math.abs(car.angle);
  }

  car.angleAcceleration = angleAcceleration;
  car.laneIndex = myCar.piecePosition.lane.endLaneIndex;
  car.radius = race.track.pieces[myCar.piecePosition.pieceIndex].radius || 0;

  if (car.pieceIndex !== myCar.piecePosition.pieceIndex) {

    car.pieceIndex = myCar.piecePosition.pieceIndex;
    car.lap = myCar.piecePosition.lap;
    car.switchSent = false;
    console.log('PIECE: ' + car.pieceIndex + ' LAP: ' + car.lap);
  }
}


function setInitialPosition(data) {

  var myCar = _.find(data, function(carData) { return carData.id.color === car.color; });

  car.pieceIndex = myCar.piecePosition.pieceIndex;
  car.inPieceDistance = myCar.piecePosition.inPieceDistance;
  car.laneIndex = myCar.piecePosition.lane.endLaneIndex;
  car.lap = myCar.piecePosition.lap;

  _.each(data, function(carData) {

    var otherCar = _.find(race.cars, function(c) {return c.id.color === carData.id.color;});

    otherCar.pieceIndex = carData.piecePosition.pieceIndex;
    otherCar.inPieceDistance = carData.piecePosition.inPieceDistance;
    otherCar.laneIndex = carData.piecePosition.lane.endLaneIndex;
    otherCar.lap = carData.piecePosition.lap;
  });
}


function react(data, tick) {

  if (tick === undefined) {
    setInitialPosition(data);
    return;
  }

  if (tick === 0) {
    sendMessage(createThrottleMessage(1));
    printCar();
    return;
  }

  if (car.isAwaitingSpawn) {
    currentTick = tick;
    sendMessage({
      msgType: 'ping',
      gameTick: currentTick
    });
    return;
  }

  var time = tick - currentTick;
  updateCar(data, time);
  updateOtherCars(data, time);
  currentTick = tick;

  sendMessage(getMessage(data));

  printCar();
}


function handleCrash(data) {

  var crashedCar = _.find(race.cars, function(carData) {return carData.id.color === data.color;});

  crashedCar.isCrashed = true;

  if (data.color !== car.color) {
    return;
  }

  car.crashes++;
  car.isAwaitingSpawn = true;
  car.speed = 0;
  car.acceleration = 0;
  car.angle = 0;
  car.angleAcceleration = 0;
  car.throttle = 0;
  car.isHitting = false;
  car.isTurboAvailableScheduled = false;
  car.isTurboAvailable = false;
}


function handleSpawn(data) {

  var crashedCar = _.find(race.cars, function(carData) {return carData.id.color === data.color;});

  crashedCar.isCrashed = false;

  if (data.color !== car.color) {
    return;
  }

  car.isAwaitingSpawn = false;
}


function handleTurboEnd(data) {

  if (data.color !== car.color) {
    return;
  }

  car.isTurboOn = false;
}


function initGame(data) {

  race = data.race;
  resetCars();
  race.track.lanes = _.sortBy(race.track.lanes, 'distanceFromCenter');
  car.guideFlagPosition = _.find(race.cars, function(carData) {return carData.id.color === car.color;}).dimensions.guideFlagPosition;
  currentTick = 0;
  RESISTANCE = 1.0;
  ticksAfterStart = 0;
  maxAngle = 0;
}

var jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {

  switch(data.msgType) {

    case 'gameStart':
      currentTick = data.gameTick;
      sendMessage(createThrottleMessage(1));
      printCar();
      return;
    case 'carPositions':
      react(data.data, data.gameTick);
      if (car.isTurboAvailableScheduled) {
        car.isTurboAvailable = true;
        car.isTurboAvailableScheduled = false;
      }
      return;

    case 'yourCar':
      car.color = data.data.color;
      break;
    case 'turboAvailable':
      car.isTurboAvailableScheduled = !car.isAwaitingSpawn;
      break;
    case 'turboEnd':
      handleTurboEnd(data.data);
      break;
    case 'crash':
      handleCrash(data.data);
      break;
    case 'spawn':
      handleSpawn(data.data);
      break;
    case 'gameInit':
      initGame(data.data);
      break;
    case 'gameEnd':
      console.log('FINISH: ' + data.data.bestLaps[0].result.millis);
      console.log('RESISTANCE: ' + round(RESISTANCE));
      console.log('MAX ANGLE: ' + round(maxAngle));
      console.log('CRASHES: ' + car.crashes);
      break;
  }

  console.log(data);
});

jsonStream.on('error', function() {
  return console.log('disconnected');
});
